/*****************************************************************************
*   Main header file For an mqtt to apcupsd daemon
*
*   Uses: apcupsd, mosquito_clients
*
*   Contact: andy@kirbyand.co.uk
*   Authors: Andy Kirby,
*   Errors & Mistakes: Somebody elses onest guv
*   Copyright: 2019
*   Licence: GPL3
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

/* includes */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <syslog.h>
#include <string.h>
#include <ctype.h>
#include <netdb.h>
#include <libconfig.h>
#include <mosquitto.h>


/* constants */
#define VER "0.1"
#define MAX 80
#define APCUPSD_PORT "3551"                                                // APCUPSD port number for monitoring
#define APCUPSD_HOST "127.0.0.1"                                           // IP address of the host running APCUPSD
#define APCUPSD_PWRCYCLE "touch /etc/apcupsd/powerfail;/sbin/apcupsd -k &" // command to power cycle items connected to UPS
#define APCUPSD_KEYFIELD 9                                                 // end column of key field
#define APCUPSD_VALUEFIELD 11                                              // start column of key field  
#define CONFFILE "/etc/ups2mqtt.conf"                                      // default config file and path
#define MQTT_PORT 1883                                                     // MQTT server port number
#define MQTT_KA 60                                                         // MQTT Keepalive ping period
#define MQTT_HOST "srvrname"                                               // MQTT broker
#define MQTT_UNAME "username"                                              // username for mqtt broker
#define MQTT_PWRD "password"                                               // password for mqtt broker
#define MQTT_LWT_TOPIC "NoT/place/node/ups2mqtt"                           // MQTT Last Will and Testament topic
#define MQTT_LWT_DEAD "dead"                                               // MQTT Last Will and Testament died value
#define MQTT_LWT_LIVE "alive"                                              // MQTT Last Will and testament alive value
#define MQTT_IN_TOPIC "NoT/place/actuator/ups2mqtt"                        // MQTT topic monitored for UPS control
#define MQTT_OUT_TOPIC "NoT/place/sensor/ups2mqtt"                         // MQTT topic status is published to


/* function prototypes */
extern void harekiri();
extern void apcupsd_get_status(int);
extern unsigned int apcupsd_get_msg(int, unsigned int, char *);
extern unsigned int apcupsd_put_msg(int, char *);
extern void my_message_callback(struct mosquitto *, void *, const struct mosquitto_message *);
extern void my_connect_callback(struct mosquitto *, void *, int);
extern void my_subscribe_callback(struct mosquitto *, void *, int, int, const int *);
extern void get_config(char *);
extern void make_default_config(char *, config_t *);
extern void make_str(const char*, char**);
extern void free_config(int);


/* Global Variables */
int daemon_done = 0;
// values for these variables allocated from config
char *ver = NULL;
char *apcupsd_port;
char *apcupsd_host = NULL;
char *apcupsd_pwrcycle = NULL;
unsigned int mqtt_port;
unsigned int mqtt_ka;
char *mqtt_host = NULL;
char *mqtt_uname = NULL;
char *mqtt_pwrd = NULL;
char *mqtt_lwt_topic = NULL;
char *mqtt_lwt_dead = NULL;
char *mqtt_lwt_live = NULL;
char *mqtt_in_topic = NULL;
char *mqtt_out_topic = NULL;


// mosquitto client instance
struct mosquitto *mosq = NULL;

