/*****************************************************************************
*   Main source file For an mqtt to apcupsd daemon
*
*   Uses: apcupsd, mosquito_clients, libconfig
*
*   Contact: andy@kirbyand.co.uk
*   Authors: Andy Kirby,
*   Errors & Mistakes: Somebody elses onest guv, pick someone, anyone will do
*   Copyright: 2019
*   Licence: GPL3
*
*   Dependencies:-
*      libmosquitto-dev
*      libconfig
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#include "ups2mqtt.h"


/* start of our daemon code */
int main(int argc, char *argv[]) {
   struct addrinfo hints, *servinfo, *p;
   int rv;
   char configfile[80];
   char txt_buff[80];
   bool clean_session = true;
   int sockfd;
   int option;
   pid_t pid, sid;
 

   /* setup default config file location */
   strncpy(configfile, CONFFILE, sizeof(configfile) - 1);

   /* do command line options */
   while((option = getopt(argc, argv, "c:")) != -1) {
      switch(option) {
         case 'c':
            strncpy(configfile, optarg, sizeof(configfile));
            printf("Using config file: %s\n", configfile);
            break;
         default:
            printf("Unknown option, try :-\n  UPS2MQTT -c <config file with path>\n\n");
            exit(EXIT_FAILURE);
      }
   }

   /* read config from config file, if it exists otherwise create a default one */
   get_config(configfile);
        
   /* Fork off the parent process */
   pid = fork();
   if (pid < 0) {
      free_config(EXIT_FAILURE);
   }

   /* If we got a good child PID, then we can exit as we are the parent process. */
   if (pid > 0) {
      printf("Daemon spawned with pid %d\n", pid);
      free_config(EXIT_SUCCESS);
   }

   /* only the child process continues hereafter */

     /* Change the file mode mask */
   umask(0);

   /* Catch, ignore and handle signals */
   signal(SIGCHLD, SIG_IGN);
   signal(SIGHUP, SIG_IGN);
   signal(SIGTTOU, SIG_IGN);
   signal(SIGTERM, harekiri);

   /* setup logging for all debug and info output here after */
   openlog ("UPS2MQTT", LOG_PID, LOG_DAEMON);
   syslog(LOG_NOTICE, "daemon started");
                
   /* Create a new SID for the child process */
   sid = setsid();
   if (sid < 0) {
      syslog (LOG_ERR, "failed to create sid");    
      free_config(EXIT_FAILURE);
   }
            
   /* Change the current working directory */
   if ((chdir("/")) < 0) {
      syslog(LOG_ERR, "Unable to change the workign directory");
      free_config(EXIT_FAILURE);
   }
        
   /* Close out the standard file descriptors that are no longer used */
   close(STDIN_FILENO);
   close(STDOUT_FILENO);
   close(STDERR_FILENO);
        
   /* Initialise MQTT client */
   mosquitto_lib_init();
   mosq = mosquitto_new(NULL, clean_session, NULL);
   if(!mosq){
      syslog(LOG_ERR, "Can not create mosquitto client, Out of memory");
      free_config(EXIT_FAILURE);
   }

   /* Setup the auth credentials for the MQTT client */
   mosquitto_username_pw_set(mosq, mqtt_uname, mqtt_pwrd);

   /* create a topic and message for LWT */
   mosquitto_will_set(mosq, mqtt_lwt_topic, strlen(mqtt_lwt_dead), mqtt_lwt_dead, 0, true);

   /* setup the MQTT client callbacks */
   //mosquitto_log_callback_set(mosq, my_log_callback);
   mosquitto_connect_callback_set(mosq, my_connect_callback);
   mosquitto_message_callback_set(mosq, my_message_callback);
   mosquitto_subscribe_callback_set(mosq, my_subscribe_callback);

   /* connect to the mqtt broker */
   /* note mosquitto_connect() does it's own adress resolution */
   if(MOSQ_ERR_SUCCESS != mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_ka)){
      syslog(LOG_ERR, "Unable to connect to MQTT broker");
      syslog(LOG_ERR, mqtt_host );
      free_config(EXIT_FAILURE);
   }

   /* zero out the hints structure and initialise it with our base info */ 
   memset(&hints, 0, sizeof hints);
   hints.ai_family = AF_UNSPEC; // use AF_INET6 to force IPv6
   hints.ai_socktype = SOCK_STREAM;

   /* get the address info for the apcupsd service */
   rv = getaddrinfo(apcupsd_host, apcupsd_port, &hints, &servinfo);
   if (rv != 0) {
       // did not get any address info cannot go any further
       sprintf(txt_buff, "getaddrinfo: %s\n", gai_strerror(rv));
       syslog (LOG_ERR, txt_buff);
       free_config(EXIT_FAILURE);
   }

   /* loop pointer p through all the results and connect to the first we can */
   // not that there can be several methods ie IPv4, IPv6 etc one should work
   // note misleading use of a for loop, re write this as a while.
   for(p = servinfo; p != NULL; p = p->ai_next) {
      sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
      if ( sockfd == -1) {
        // unable to create that socket
        continue;
      }

      connect(sockfd, p->ai_addr, p->ai_addrlen);
      if ( sockfd == -1) {
        // unable to open that socket
        close(sockfd);
        continue;
      }

      // if we get here, we must have connected, loop no more
      break; 
   }

   if (p == NULL) {
      // looped off the end of the list with no connection
      syslog(LOG_ERR, "connection with the apcupsd server failed");
      free_config(EXIT_FAILURE);
   }

   // all done with this structure we can rely on sockfd now
   freeaddrinfo(servinfo); 

   // log that we connected succesfully
   syslog(LOG_NOTICE, "APCUPSD connect succesful"); 

   // service the mosquitto client code via its own thread
   if(mosquitto_loop_start(mosq) == MOSQ_ERR_NOT_SUPPORTED) {
      syslog(LOG_ERR, "Mosquitto threaded client support is not available");
   }

   // sleep for a little to let the mosquitto client establish itself.
   sleep(1);

   // until something unsets the daemon_done flag do UPS reporting via MQTT
   while(!daemon_done) {
      // read the ups status
      apcupsd_get_status(sockfd);

      // sleep for a little while then go around again
      sleep(60);
   }

   // do clear up and exit
   syslog(LOG_NOTICE, "daemon stopping by command");
   mosquitto_loop_stop(mosq, true);
   close(sockfd);   
   free_config(EXIT_SUCCESS);
}


/* utility routines */
void harekiri(){
   daemon_done = 1;

   return;
}

void apcupsd_get_status(int sockfd) {
   char buff[80];
   char newtopic[170];
   unsigned int eostat = 1;
   char *charindex;
   int doneflag = false;

   // tell apcupsd to read off it's status
   sprintf(buff, "status");
   apcupsd_put_msg(sockfd, buff);

   // until apcupsd sends a zero length message indcating end of the status messages
   while(eostat > 0) {
      // get a message from the socket
      eostat = apcupsd_get_msg(sockfd, sizeof(buff), buff);

      // if we got a message process it
      if(eostat > 0) {
         // reset the key field processing flag
         doneflag = false;

         // make all the message lowercase
         charindex = buff;
         while(*charindex != '\0'){
            *charindex = (char) tolower(*charindex);
            charindex++;
         }

         // APC status mesages are column aligned : delimited key value pairs.
         // key field starts at the begining and runs until the delimiter : at column APCUPSD_KEYFIELD packed with spaces for short keys
         // start at APCUPSD_KEYFILED and workign towards the begining over write with nulls till a non space or delimiter is encountered.
         charindex = &buff[APCUPSD_KEYFIELD];
         while(!doneflag){
            if(*charindex == ' ' || *charindex == ':') {
               *charindex = '\0';
               charindex--;
            } else {
               doneflag = true;
            }
         }

         // create a new topic from the current topic base plus the key field
         strncpy(newtopic, mqtt_out_topic, sizeof(buff));
         strcat(newtopic, "/");
         strncat(newtopic, buff, sizeof(newtopic));

         // value field starts at APCUPSD_VALUEFIELD and runs till the end of the string
         mosquitto_publish(mosq, NULL, newtopic, strlen(&buff[APCUPSD_VALUEFIELD]), &buff[APCUPSD_VALUEFIELD], 0, false);
          
      }
   }

   return;
}


unsigned int apcupsd_get_msg(int sockfd, unsigned int maxmsg, char *msg) {
   unsigned int msg_len = 0;
   int dump_count;
   char dump;

   // check socket is available
   if (sockfd != -1) {   
      // get first two bytes of message and assemble into msg_len
      read(sockfd, &msg_len, 2);
      msg_len = ntohs(msg_len);

      // check if message is zero length and if so dont bother reading any further
      // NB last message of the run is always 0 length, this indicates end of messages
      if(msg_len > 0) {
         // check message length against the max we can rx
         if(msg_len < (maxmsg - 1)) {
            // read all of the message into msg 
            read(sockfd, msg, msg_len);
            // null terminate the string that is now in msg
            msg[msg_len - 1] = 0x00; 
         } else {
            // read as much as safely possible into msg
            read(sockfd, msg, maxmsg - 1);
            // null terminate the string that is now in msg
            msg[maxmsg - 1] = 0x00;
            // dump the rest as we can not do anythign useful with it
            for( dump_count = msg_len - maxmsg; dump_count >=0; dump_count--){
               // read a byte from the socket
               read(sockfd, &dump, 1);
            }
         }
      }
   }

   // return the length of the message that has been read
   return(msg_len);
}


unsigned int apcupsd_put_msg(int sockfd, char *msg) {
   unsigned msg_len = 0;
   unsigned net_msg_len = 0;

   // check socket is available
   if (sockfd != -1) {
      // get length of message to send
      msg_len = strlen(msg);

      // send length of the message in the first two bytes
      net_msg_len = htons(msg_len);
      write(sockfd, &net_msg_len, 2);

      // send msg
      write(sockfd, msg, msg_len);
   }


   // return the legth of the message that has been sent
   return(msg_len);
}


void get_config(char *config_file) {
   config_setting_t *setting;
   struct stat finfo;
   config_t cfg;
   config_t *cf;

   // initialise the config store
   cf = &cfg;
   config_init(cf);
  
   if( stat(config_file, &finfo) ) {
      // config file does not exist create one
      make_default_config(config_file, cf);
   } else {
      // read config from config file, if syntax errors report them and exit
      if (!config_read_file(cf, config_file)) {
         fprintf(stderr, "%s:%d - %s\n", config_error_file(cf), config_error_line(cf), config_error_text(cf));
         config_destroy(cf);
         exit(EXIT_FAILURE);
      }
   }
  
   //char *ver;
   setting = config_lookup(cf, "version");
   if(setting) {
      make_str(config_setting_get_string(setting), &ver);
   }

   //char *apcupsd_port;
   setting = config_lookup(cf, "apcupsd.port");
   if(setting) {
      make_str(config_setting_get_string(setting), &apcupsd_port);
   }

   //char *apcupsd_host;
   setting = config_lookup(cf, "apcupsd.host");
   if(setting) {
      make_str(config_setting_get_string(setting), &apcupsd_host);
   }

   //char *apcupsd_pwrcycle;
   setting = config_lookup(cf, "apcupsd.pwrcycle");
   if(setting) {
      make_str(config_setting_get_string(setting), &apcupsd_pwrcycle);
   }

   //unsigned int mqtt_port;
   setting = config_lookup(cf, "mqtt.port");
   if(setting) {
      mqtt_port = config_setting_get_int(setting);
   }

   //unsigned int mqtt_ka;
   setting = config_lookup(cf, "mqtt.keepalive");
   if(setting) {
      mqtt_ka = config_setting_get_int(setting);
   }

   //char *mqtt_host;
   setting = config_lookup(cf, "mqtt.host");
   if(setting) {
      make_str(config_setting_get_string(setting), &mqtt_host);
   }

   //char *mqtt_uname;
   setting = config_lookup(cf, "mqtt.username");
   if(setting) {
      make_str(config_setting_get_string(setting), &mqtt_uname);
   }

   //char *mqtt_pwrd;
   setting = config_lookup(cf, "mqtt.password");
   if(setting) {
      make_str(config_setting_get_string(setting), &mqtt_pwrd);
   }

   //char *mqtt_lwt_topic;
   setting = config_lookup(cf, "mqtt.lwt_topic");
   if(setting) {
      make_str(config_setting_get_string(setting), &mqtt_lwt_topic);
   }

   //char *mqtt_lwt_dead;
   setting = config_lookup(cf, "mqtt.lwt_dead");
   if(setting) {
      make_str(config_setting_get_string(setting), &mqtt_lwt_dead);
   }

   //char *mqtt_lwt_live;
   setting = config_lookup(cf, "mqtt.lwt_live");
   if(setting) {
      make_str(config_setting_get_string(setting), &mqtt_lwt_live);
   }

   //char *mqtt_in_topic;
   setting = config_lookup(cf, "mqtt.in_topic");
   if(setting) {
      make_str(config_setting_get_string(setting), &mqtt_in_topic);
   }

   //char *mqtt_out_topic;
   setting = config_lookup(cf, "mqtt.out_topic");
   if(setting) {
      make_str(config_setting_get_string(setting), &mqtt_out_topic);
   }

   config_destroy(cf);
   return;
}


void make_default_config(char *config_file, config_t *cf) {
   config_setting_t *root, *setting, *mqtt, *apcupsd;

   // create base structure root to hold values, and two subgroups apcupsd and mqtt
   root = config_root_setting(cf);
   apcupsd = config_setting_add(root, "apcupsd", CONFIG_TYPE_GROUP);
   mqtt = config_setting_add(root, "mqtt", CONFIG_TYPE_GROUP);

   // fill cfg with default values
   //char *ver;
   setting = config_setting_add(root, "version", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, VER);

   //unsigned int apcupsd_port;
   setting = config_setting_add(apcupsd, "port", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, APCUPSD_PORT);

   //char *apcupsd_host;
   setting = config_setting_add(apcupsd, "host", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, APCUPSD_HOST);

   //char *apcupsd_pwrcycle;
   setting = config_setting_add(apcupsd, "pwrcycle", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, APCUPSD_PWRCYCLE);

   //unsigned int mqtt_port;
   setting = config_setting_add(mqtt, "port", CONFIG_TYPE_INT);
   config_setting_set_int(setting, MQTT_PORT);

   //unsigned int mqtt_ka; config_setting_set_int
   setting = config_setting_add(mqtt, "keepalive", CONFIG_TYPE_INT);
   config_setting_set_int(setting, MQTT_KA);

   //char *mqtt_host;
   setting = config_setting_add(mqtt, "host", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, MQTT_HOST);

   //char *mqtt_uname;
   setting = config_setting_add(mqtt, "username", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, MQTT_UNAME);

   //char *mqtt_pwrd;
   setting = config_setting_add(mqtt, "password", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, MQTT_PWRD);

   //char *mqtt_lwt_topic;
   setting = config_setting_add(mqtt, "lwt_topic", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, MQTT_LWT_TOPIC);

   //char *mqtt_lwt_dead;
   setting = config_setting_add(mqtt, "lwt_dead", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, MQTT_LWT_DEAD);

   //char *mqtt_lwt_live;
   setting = config_setting_add(mqtt, "lwt_live", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, MQTT_LWT_LIVE);

   //char *mqtt_in_topic;
   setting = config_setting_add(mqtt, "in_topic", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, MQTT_IN_TOPIC);

   //char *mqtt_out_topic;
   setting = config_setting_add(mqtt, "out_topic", CONFIG_TYPE_STRING);
   config_setting_set_string(setting, MQTT_OUT_TOPIC);

   // write cfg back to config_file if unable to create one exit with errors
   if(CONFIG_FALSE == config_write_file(cf, config_file)) {
      fprintf(stderr, "Unable to create config file %s, exiting\n",config_file);
      config_destroy(cf);
      exit(EXIT_FAILURE);
   }

   fprintf(stderr, "New config file writen to %s\n",config_file);

   return;
}


// free up the memory allocated for config during make_config()
void free_config(int status) {
   free(ver);
   free(apcupsd_port);
   free(apcupsd_host);
   free(apcupsd_pwrcycle);
   free(mqtt_host);
   free(mqtt_uname);
   free(mqtt_pwrd);
   free(mqtt_lwt_topic);
   free(mqtt_lwt_dead);
   free(mqtt_lwt_live);
   free(mqtt_in_topic);
   free(mqtt_out_topic);

   if(mosq != NULL) {
      mosquitto_destroy(mosq);
      mosquitto_lib_cleanup();
   }

   exit(status);
}

// allocate space for a string, put the pointer for the space into global
// and copy the string to the allocated space
void make_str(const char* cfg_str, char** global) {
   int len;

   len = strlen(cfg_str) + 1;
   *global = malloc(len);
   if( *global != NULL ) {
      strncpy(*global, cfg_str, len);
   }

   return;
}


/* MQTT Callbacks */
// called by the mqtt client when an inbound message is received
void my_message_callback(struct mosquitto *mosq, void *userdata, const struct mosquitto_message *message) {
   char buf[256];
   int payloadval;

   if(message->payloadlen == 0) {
      sprintf(buf,"No payload received for topic: %s", message->topic);
      syslog(LOG_DEBUG, buf);
   } else {
      payloadval = atoi(message->payload);
      switch(payloadval) {
         case 0:
            break;
         case 1:
            sprintf(buf,"Powercycling the UPS attached devices");
            system(apcupsd_pwrcycle);
            syslog(LOG_DEBUG, buf);
            break;
         default:
            sprintf(buf,"Unknown instruction received: %d", payloadval);
            syslog(LOG_DEBUG, buf);
            break;
      }
   }

   return; 
}

// called by the mqtt client when a connection has been succesfuly made
void my_connect_callback(struct mosquitto *mosq, void *userdata, int result) {
   if(!result){
      syslog(LOG_NOTICE, "MQTT connect succesful"); 
      /* publish alive message to broker LWT topic */
      mosquitto_publish(mosq, NULL, mqtt_lwt_topic, strlen(mqtt_lwt_live), mqtt_lwt_live, 0, true);
      /* subscribe to the topics we want to monitor */
      mosquitto_subscribe(mosq, NULL, mqtt_in_topic, 0);
   }else{
      syslog(LOG_ERR, "MQTT connect failed");
   }

   return;
}

// called by the mqtt client whan a topic has been succefuly subscribed to
void my_subscribe_callback(struct mosquitto *mosq, void *userdata, int mid, int qos_count, const int *granted_qos) {
   int i;
   char buf[80];

   sprintf(buf,"Subscribed (mid: %d): %d", mid, granted_qos[0]);
   for(i=1; i<qos_count; i++){
      sprintf(buf + strlen(buf),", %d", granted_qos[i]);
   }

   syslog(LOG_NOTICE, buf);

   return;  
}

