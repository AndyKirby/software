/*****************************************************************************
*   README file For an mqtt to apcupsd daemon
*
*   Uses: apcupsd, mosquito_clients
*
*   Contact: andy@kirbyand.co.uk
*   Authors: Andy Kirby,
*   Errors & Mistakes: Somebody elses onest guv
*   Copyright: 2019
*   Licence: GPL3
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

To use:-

Compile the daemon copy it to the usual daemons path for your linux 
distribution, add an init.rc script (or systemd script) and copy the example 
dev.conf file to /etc/ups2mqtt.conf then edit the conf file to suit your local.

If you run the daemon without a config file the first time it will create its own 
default config file for you, just in case you missed that step.

You can specify an alternative config file to use wih the damon via the -c 
command line parameter.

The daemon periodicaly reads your APC ups status via apcupsd's network 
information service then publishes this to your mqtt broker using the topic 
prefix specified in the config file.

If you have enabled apcupsd to power cycle your attached devices (see apcupsd) 
then publishing a command to the configured mqtt topic prefix will cause the 
daemon to instruct apcupsd to powercycle the UPS.

Compilation and operation depends on :-

* libconfig
* libmosquitto

Dependancies can be installed on Debian using:-

apt install libconfig-dev libmosquittopp-dev 




